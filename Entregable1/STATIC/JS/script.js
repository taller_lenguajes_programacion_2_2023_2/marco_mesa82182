const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const container = document.querySelector(".container");

sign_up_btn.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
});

sign_in_btn.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
});

// Registro de usuario en localStorage
const registerButton = document.querySelector("#registerButton");
registerButton.addEventListener("click", () => {
  const username = document.querySelector(".sign-up-form input[type='text']").value;
  const email = document.querySelector(".sign-up-form input[type='email']").value;
  const password = document.querySelector(".sign-up-form input[type='password']").value;

  // Verifica que los campos no estén vacíos
  if (username && email && password) {
    // Crea un objeto de usuario
    const user = { username, email, password };

    // Almacena el usuario en localStorage
    localStorage.setItem('user', JSON.stringify(user));

   
    document.querySelector(".sign-up-form input[type='text']").value = '';
    document.querySelector(".sign-up-form input[type='email']").value = '';
    document.querySelector(".sign-up-form input[type='password']").value = '';

    // Cambia al modo de inicio de sesión
    container.classList.remove("sign-up-mode");
  } else {
    alert('Por favor, completa todos los campos para registrarte.');
  }
});

// Inicio de sesión desde localStorage
const loginButton = document.querySelector("#loginButton");
loginButton.addEventListener("click", () => {
  const storedUser = localStorage.getItem('user');
  if (storedUser) {
    const username = document.querySelector(".sign-in-form input[type='text']").value;
    const password = document.querySelector(".sign-in-form input[type='password']").value;
    const user = JSON.parse(storedUser);

    // Comprueba si las credenciales son correctas
    if (username === user.username && password === user.password) {
        alert('Inicio de sesión exitoso. Haga clic en Aceptar para continuar.');

  // Utiliza setTimeout para redirigir
  setTimeout(function() {
    window.location.href = 'principal.html';
  }, 100); // Espera 100 milisegundos antes de redirigir
} else {
  alert('Credenciales incorrectas. Inténtalo de nuevo.');
}
  }
});


