import sqlite3
import pandas as pd

# Clase conexión 
class Conexion:
    def __init__(self, db_path):
        self.db_path = db_path
        self.connection = None
        self.cursor = None

    def conectar(self):
        try:
            self.connection = sqlite3.connect(self.db_path)
            self.cursor = self.connection.cursor()
            print("Conexión a la base de datos establecida.")
        except sqlite3.Error as error:
            print("Error al conectar a la base de datos:", error)

    def desconectar(self):
        if self.connection:
            self.connection.close()
            print("Conexión a la base de datos cerrada.")

    def ejecutar_sql(self, sql, values=None):
        try:
            if values:
                self.cursor.execute(sql, values)
            else:
                self.cursor.execute(sql)
            self.connection.commit()
            print("Comando SQL ejecutado correctamente.")
        except sqlite3.Error as error:
            print("Error al ejecutar el comando SQL:", error)



#Clase Padre

class Musica:

    """
    La clase Musica representa una canción

    Attributes:
        nombre (str): Nombre de la canción.
        artista (str): Nombre del artista.
        album (str): Nombre del álbum.
        duracion (int): Duración de la canción en segundos.
        genero (str): Género de la canción.
        anio (int): Año de lanzamiento de la canción.
    """

    def __init__(self, nombre, artista, album, duracion, genero, anio):

        """
        Inicializa una nueva instancia de la clase Musica.

        Args:
            nombre (str): Nombre de la canción.
            artista (str): Nombre del artista.
            album (str): Nombre del álbum.
            duracion (int): Duración de la canción en segundos.
            genero (str): Género de la canción.
            anio (int): Año de lanzamiento de la canción.
        """

        self.nombre = nombre
        self.artista = artista
        self.album = album
        self.duracion = duracion
        self.genero = genero
        self.anio = anio

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nombre):
        self.nombre = nombre

    def get_artista(self):
        return self.artista

    def set_artista(self, artista):
        self.artista = artista

    def get_album(self):
        return self.album

    def set_album(self, album):
        self.album = album

    def get_duracion(self):
        return self.duracion

    def set_duracion(self, duracion):
        self.duracion = duracion

    def get_genero(self):
        return self.genero

    def set_genero(self, genero):
        self.genero = genero

    def get_anio(self):
        return self.anio

    def set_anio(self, anio):
        self.anio = anio

    def crear_registro(self, conexion):
        values = (self.nombre, self.artista, self.album, self.duracion, self.genero, self.anio)
        conexion.ejecutar_sql("INSERT INTO musica VALUES (?, ?, ?, ?, ?, ?)", values)

    def leer_registro(self, conexion):
        sql = f"SELECT * FROM musica WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.cursor.execute(sql)
        return conexion.cursor.fetchone()

    def actualizar_registro(self, conexion):
        sql = f"UPDATE musica SET album = '{self.album}', duracion = {self.duracion}, genero = '{self.genero}', anio = {self.anio} WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.ejecutar_sql(sql)

    def eliminar_registro(self, conexion):
        sql = f"DELETE FROM musica WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.ejecutar_sql(sql)

    @staticmethod
    def migrar_desde_excel(conexion, excel_file_path):
        df = pd.read_excel(excel_file_path, sheet_name=0)
        for _, row in df.iterrows():
            musica = Musica(row['nombre'], row['artista'], row['album'], row['duracion'], row['genero'], row['anio'])
            musica.crear_registro(conexion)
    

#Clase Hijo

class Rock(Musica):

    """
    Esta clase representa una canción de rock, que es un subtipo de canción musical.

    Attributes:
        subgenero (str): Subgénero de la canción de rock.
        formato (str): Formato de la canción de rock.
        letra (str): Letra de la canción de rock.
        instrumentos (str): Instrumentos usados en la canción de rock.
        ritmo (str): Ritmo de la canción de rock.
        idioma (str): Idioma de la canción de rock.
    """

    def __init__(self, nombre, artista, album, duracion, genero, anio,
                 subgenero, formato, letra, instrumentos, ritmo, idioma):
        
        """
        Inicializa una nueva instancia de la clase Rock.

        Args:
            nombre (str): Nombre de la canción.
            artista (str): Nombre del artista.
            album (str): Nombre del álbum.
            duracion (int): Duración de la canción en segundos.
            genero (str): Género de la canción.
            anio (int): Año de lanzamiento de la canción.
            subgenero (str): Subgénero de la canción de rock.
            formato (str): Formato de la canción de rock.
            letra (str): Letra de la canción de rock.
            instrumentos (str): Instrumentos utilizados en la canción de rock.
            ritmo (str): Ritmo de la canción de rock.
            idioma (str): Idioma de la canción de rock.
        """

        super().__init__(nombre, artista, album, duracion, genero, anio)
        self.subgenero = subgenero
        self.formato = formato
        self.letra = letra
        self.instrumentos = instrumentos
        self.ritmo = ritmo
        self.idioma = idioma

    def get_subgenero(self):
        return self.subgenero

    def set_subgenero(self, subgenero):
        self.subgenero = subgenero

    def get_formato(self):
        return self.formato

    def set_formato(self, formato):
        self.formato = formato

    def get_letra(self):
        return self.letra

    def set_letra(self, letra):
        self.letra = letra

    def get_instrumentos(self):
        return self.instrumentos

    def set_instrumentos(self, instrumentos):
        self.instrumentos = instrumentos

    def get_ritmo(self):
        return self.ritmo

    def set_ritmo(self, ritmo):
        self.ritmo = ritmo

    def get_idioma(self):
        return self.idioma

    def set_idioma(self, idioma):
        self.idioma = idioma

    def crear_registro(self, conexion):
        super().crear_registro(conexion)
        values = (self.nombre, self.subgenero, self.formato, self.letra, self.instrumentos, self.ritmo, self.idioma)
        conexion.ejecutar_sql("INSERT INTO rock VALUES (?, ?, ?, ?, ?, ?, ?)", values)
    
    def leer_registro(self, conexion):
        sql = f"SELECT * FROM rock WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.cursor.execute(sql)
        return conexion.cursor.fetchone()

    def actualizar_registro(self, conexion):
        sql = f"UPDATE rock SET subgenero = '{self.subgenero}', formato = '{self.formato}', letra = '{self.letra}', instrumentos = '{self.instrumentos}', ritmo = '{self.ritmo}', idioma = '{self.idioma}' WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.ejecutar_sql(sql)

    def eliminar_registro(self, conexion):
        sql = f"DELETE FROM rock WHERE nombre = '{self.nombre}' AND artista = '{self.artista}'"
        conexion.ejecutar_sql(sql)

    @staticmethod
    def migrar_desde_excel(conexion, excel_file_path):
        df = pd.read_excel(excel_file_path, sheet_name=0)
        for _, row in df.iterrows():
            # ... (extraer datos específicos para la clase Rock)
            rock_song = Rock(nombre, subgenero, formato, letra, instrumentos, ritmo, idioma)
            rock_song.crear_registro(conexion)



# Crear una instancia de la clase Conexion
conexion = Conexion('baseDeDatos.db')
conexion.conectar()

# Consulta para crear la tabla "musica"
consulta_creacion_tabla_musica = '''
CREATE TABLE IF NOT EXISTS musica (
    nombre TEXT,
    artista TEXT,
    album TEXT,
    duracion INTEGER,
    genero TEXT,
    anio INTEGER
);
'''

# Ejecutar la consulta para crear la tabla "musica"
conexion.ejecutar_sql(consulta_creacion_tabla_musica)

consulta_creacion_tabla_rock = '''
CREATE TABLE IF NOT EXISTS rock (
    nombre TEXT,
    artista TEXT,
    album TEXT,
    duracion INTEGER,
    genero TEXT,
    anio INTEGER,
    subgenero TEXT,
    formato TEXT,
    letra TEXT,
    instrumentos TEXT,
    ritmo TEXT,
    idioma TEXT
);
'''

# Ejecutar la consulta para crear la tabla "musica"
conexion.ejecutar_sql(consulta_creacion_tabla_rock)

# Llamar a la función para migrar datos desde un archivo Excel para la clase Musica
Musica.migrar_desde_excel(conexion, 'musica.xlsx')

# Llamar a la función para migrar datos desde un archivo Excel para la clase Rock
Rock.migrar_desde_excel(conexion, 'rock.xlsx')

# Desconectar la conexión a la base de datos
conexion.desconectar()


# Prueba de uso
rock_song = Rock("One", "Metallica", "Album 1", 240, "Rock", 2000,
                 "Hard Rock", "MP3", "This is the lyrics", "Guitar, Drums", "Fast", "English")


print(rock_song.get_nombre())
print(rock_song.get_subgenero())

