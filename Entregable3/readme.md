Para ingresar a la página principal primero se encontrará con una ventana de login y registro.
En caso de ser usuario nuevo debe registrarse con un nombre de usuario y una contraseña tal como lo indican 
las instrucciones del formulario.

Una vez tenga una cuenta registrada en la página, realice el login, allí encontrará una página de Home y un navbar,
mediante este se podrá dirigir a la plantilla de productos para ingresar uno nuevo.

Para cerrar sesión simplemente debe dar click en el apartado "Logout" de la navbar.