# Marco Antonio Mesa Cardona

Taller de Lenguajes de Programación 2

1- Ajustes realizados a la aplicación para el cuarto entregable

Paso a paso:
1. python manage.py runserver
2. Instalar las dependencias necesarias (Incluyendo pillow para el correcto despliegue)
3. Para iniciar sesión en una cuenta con status de admin use las siguientes credenciales:
   Usuario: admin
   Contraseña: Admin1234$
4. Para registrar un usuario nuevo debe hacerse desde (http://127.0.0.1:8000/admin)
5. Para ingresar un nuevo producto puede hacerlo también desde (http://127.0.0.1:8000/admin)
5. Una vez que esté logeado podrá interactuar con todas las funcionalidades de la app, como agregar al carrito, ver la cuenta y hacer checkout.