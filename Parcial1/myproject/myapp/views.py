from django.shortcuts import render, redirect
from .forms import LoginRegistroForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

def index(request):
    if request.method == 'POST':
        form = LoginRegistroForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']

            # Intenta autenticar al usuario
            user = authenticate(request, username=username, password=password)

            if user is not None:
                # Si el usuario existe, inicia sesión
                login(request, user)
            else:
                # Si el usuario no existe, crea una nueva cuenta
                user = User.objects.create_user(username=username, password=password, email=email)
                login(request, user)

            # Obtiene la URL a la que el usuario debe ser redirigido
            next_url = request.POST.get('next', '/')
            return redirect(next_url)  # Redirige a la URL especificada en 'next'
    else:
        form = LoginRegistroForm()
        
    return render(request, 'index.html', {'form': form})